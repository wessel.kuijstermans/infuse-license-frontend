// Ref: https://next-auth.js.org/getting-started/typescript#module-augmentation

import {DefaultSession, DefaultUser} from "next-auth"
import {JWT, DefaultJWT} from "next-auth/jwt"

declare module "next-auth" {
   interface Session extends DefaultSession {
      user: {
         name: string;
         email: string;
         companyName: string;
         image: string;
         id: string;
         role: string;
         firstLogin: boolean;
      }
      accessToken: string;
   }

   interface User extends DefaultUser {
      role: string,
      companyName: string,
      firstLogin: boolean,
   }
}

declare module "next-auth/jwt" {
   interface JWT extends DefaultJWT {
      id: string;
      role: string,
      companyName: string,
      firstLogin: boolean,
      accessToken: string;
   }
}