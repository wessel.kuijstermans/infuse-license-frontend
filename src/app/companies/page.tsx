'use client'

import 'tui-grid/dist/tui-grid.css';
import 'react-toastify/dist/ReactToastify.css';
import React, {useEffect, useState} from "react";
import {useRouter} from "next/navigation";
import CompanyGrid from "@/app/companies/CompanyGrid";
import Button from "@/app/components/elements/Button";
import TextBox from "@/app/components/elements/TextBox";
import {toast, ToastContainer} from "react-toastify";
import {useSession} from "next-auth/react";
import {Company} from "@/app/lib/Company";

const CompaniesPage: React.FC = () => {
    const [creating, setCreating] = useState(false);
    const {data: session} = useSession();
    const router = useRouter();
    const [companies, setCompanies] = useState<Company[] | null>(null);

    useEffect(() => {
        fetchCompanies()
    }, []);

    function fetchCompanies() {
        if (session) {
            fetch(process.env.API_URL + "/companies", {
                method: "GET",
                headers: {
                    Authorization: session.accessToken
                },
                cache: "no-cache"
            }).then((response) => response.json()).then((data) => {
                console.log(data)
                setCompanies(data)
            }).catch((error) => {
                console.error("Error fetching companies:", error);
            });
        }
    }

    function onSubmit() {
        const name = document.getElementById("name") as HTMLInputElement;
        if (name.value == "") {
            toast.error("Please enter a name", {autoClose: 2000})
            return;
        }
        if (session) {
            fetch(process.env.API_URL + "/companies/" + name.value, {
                method: "POST",
                headers: {
                    Authorization: session.accessToken
                }
            }).then((response) => {
                if (response.ok) {
                    fetchCompanies()
                    toast.success("Company created successfully!", {autoClose: 2000})
                } else {
                    toast.error("Error creating company", {autoClose: 2000})
                }
            })
            setCreating(false);
        }

    }

    function keyboardControls(event: React.KeyboardEvent<HTMLDivElement>) {
        if (event.key === "Enter") {
            onSubmit()
        } else if (event.key === "Escape") {
            setCreating(false)
        }
    }

    return (
        <div className={"flex flex-row w-full"}>
            <ToastContainer/>
            <div className={"tui-grid m-3 w-1/6"}>
                <CompanyGrid companies={companies}/>
            </div>
            {creating ? (
                <div className={"flex flex-col w-auto m-4"}>
                    <div>
                        <h1 className={"text-4xl"}>Create Company</h1>
                        <TextBox onKeyDown={(event) => keyboardControls(event)} id={"name"} labelText={"Company Name"}/>
                    </div>
                    <div className={"flex flex-row space-x-3 mt-4"}>
                        <Button className={"w-fit"} variant={"success"}
                                onClick={() => onSubmit()}>Confirm</Button>
                        <Button className={"w-fit"} variant={"danger"}
                                onClick={() => setCreating(false)}>Cancel</Button>
                    </div>
                </div>
            ) : (<div className={"flex w-full m-4"}>
                <Button className={"w-fit h-fit"} variant={"success"}
                        onClick={() => setCreating(true)}>Create</Button>
            </div>)}
        </div>
    );
};

export default CompaniesPage;