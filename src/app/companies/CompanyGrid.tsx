"use client"

import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow,} from 'tui-grid/types/options';
import React, {useState, useRef} from 'react';
import {useSession} from 'next-auth/react';
import Button from "@/app/components/elements/Button";
import {Company} from "@/app/lib/Company";
import 'react-toastify/dist/ReactToastify.css';
import {toast, ToastContainer} from "react-toastify";

interface IProps {
    companies: Company[] | null
}

const CompanyGrid = ({companies}: IProps) => {
    const {data: session} = useSession()
    const gridRef = useRef<Grid>(null);
    const [selectedCompany, setSelectedCompany] = useState<string | null>(null)

    function changeSelected(ev: any) {
        const rowKey = ev.rowKey
        const row = gridRef?.current?.getInstance().getRow(rowKey)
        if (row?.company) {
            setSelectedCompany(`${row.company}`)
        }
        console.log(row?.company)
    }

    function deleteCompany() {
        console.log("called!")
        if (session) {
            fetch(process.env.API_URL + "/companies/" + selectedCompany, {
                method: "DELETE",
                headers: {
                    "Authorization": session.accessToken
                }
            }).then(response => {
                if (response.ok) {
                    toast.success("Company Successfully Deleted", {autoClose: 2000})
                } else {
                    toast.error("Failed to Delete Company", {autoClose: 2000})
                }
            })
        }
    }

    if (companies) {
        const data: OptRow[] = companies.map((company) => ({
            id: company.id,
            company: company.name
        }));


        const columns: OptColumn[] = [
            {name: 'company', header: 'Company', sortable: true, filter: 'text'}
        ];

        return (
            <div className={"w-full"}>
                <ToastContainer/>
                <Grid ref={gridRef} data={data} columns={columns}
                      header={{align: 'left'}} scrollX={false} scrollY={false}
                      onFocusChange={(ev) => changeSelected(ev)}/>
                <Button disabled={!selectedCompany} variant={"danger"} onClick={() => deleteCompany()}
                        className={`cursor-pointer disabled:cursor-default disabled:bg-red-400`}>Delete</Button>
            </div>
        )
    }

    return <div>Loading...</div>;
}

export default CompanyGrid;