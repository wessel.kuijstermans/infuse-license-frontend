export enum ContractStatus {
   VALID = "VALID",
   CANCELED = "CANCELED",
   REVOKED = "REVOKED",
}