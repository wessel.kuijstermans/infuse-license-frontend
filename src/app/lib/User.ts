export interface User {
   email: string;
   name: string;
   companyName: string;
   role: string;
   selectedRole: string;
}