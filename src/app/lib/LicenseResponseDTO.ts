import {ProductRelease} from "@/app/lib/ProductRelease";
import {LicenseFunction} from "@/app/lib/LicenseFunction";

export interface LicenseResponseDTO {
    id: string;
    startDate: Date;
    endDate: Date;
    contractStatus: string;
    company: string;
    functions: LicenseFunction[]
    product: string;
    maxRelease: string;
    licenseType: string;


}