import {ContractStatus} from "@/app/lib/ContractStatus";
import {LicenseType} from "@/app/lib/LicenseType";
import {ProductRelease} from "@/app/lib/ProductRelease";
import {Company} from "@/app/lib/Company";
import {LicenseFunction} from "@/app/lib/LicenseFunction";

export interface License {
    id: string,
    startDate: Date,
    endDate: Date,
    contractStatus: ContractStatus,
    licenseType: LicenseType,
    maxRelease: ProductRelease,
    company: Company
    licenseFunctions: LicenseFunction[]
}