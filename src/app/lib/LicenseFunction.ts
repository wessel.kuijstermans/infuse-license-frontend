import {ContractStatus} from "@/app/lib/ContractStatus";
import {Function} from "@/app/lib/Function";

export type LicenseFunction = {
    id: number;
    function: Function;
    contractStatus: ContractStatus;
    endDate: Date;
}