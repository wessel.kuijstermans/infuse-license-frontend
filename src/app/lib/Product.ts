import {LicenseFunction} from "@/app/lib/LicenseFunction";
import {ProductRelease} from "@/app/lib/ProductRelease";

export type Product = {
    id: number;
    name: string;
    functions: Function[];
    productReleases: ProductRelease[];
}