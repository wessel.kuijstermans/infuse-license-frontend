export enum Version {
   TRIAL = "TRIAL",
   STANDARD = "STANDARD",
   PRO = "PRO"
}