import {Company} from "@/app/lib/Company";
import {ProductRelease} from "@/app/lib/ProductRelease";

export interface LicenseRequestDTO {
    email: string,
    company: Company,
    months: number,
    functions: Function[],
    maxRelease: ProductRelease,
    startDate: Date
}