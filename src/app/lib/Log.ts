import {Message} from "@/app/lib/Message";

export interface Log {
   id: string;
   licenseId: string;
   licenseExpirationDate: Date;
   ipAdress: string;
   message: Message;
   createdAt: number[];
}