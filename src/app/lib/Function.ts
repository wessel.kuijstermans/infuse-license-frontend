import {Product} from "@/app/lib/Product";

export type Function = {
    id: number;
    name: string;
    product: Product;
}