import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow, OptCellRenderer} from 'tui-grid/types/options';
import {User} from '@/app/lib/User';
import React, {useState, useEffect, useRef} from 'react';
import {useSession} from 'next-auth/react';
import Button from "@/app/components/elements/Button";
import {number} from "prop-types";
import {grid} from "@mui/system";

function UserGrid() {
   const {data: session} = useSession();
   const [users, setUsers] = useState<User[] | null>(null);
   const gridRef = useRef<Grid>(null);

   const buttonDisabled = () => {
      const selectedRows = gridRef.current?.getInstance().getCheckedRows();
      return !(selectedRows && selectedRows.length > 0);
   }

   const handleDelete = async () => {
      const selectedRows = gridRef.current?.getInstance().getCheckedRows();
      if (selectedRows && session) {
         selectedRows.forEach((row) => {
            fetch(process.env.API_URL + '/users/' + row.email, {
               method: 'DELETE',
               headers: {
                  Authorization: session.accessToken
               },
            })
            .then((response) => response.json())
            .then((data) => {
               console.log(data);
            })
            .catch((error) => {
               console.error('Error deleting user:', error);
            });
         });
      }
   }

   const handleUpdate = async () => {
      const rows = gridRef.current?.getInstance().getData();
      if (rows && session) {
         rows.forEach((row) => {
            if (row.role !== row.originalRole) {
               console.log(row.rowKey + ": " + row.role + " " + row.originalRole)
               fetch(process.env.API_URL + '/users/updaterole/' + row.email, {
                  method: 'PUT',
                  headers: {
                     Authorization: session.accessToken,
                     'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                     role: row.role
                  })
               })
               .then((response) => {
                  console.log(response.status)
                  if (response.ok) {
                     if (row.rowKey) {
                        const rowKey = Number(row.rowKey);
                        gridRef.current?.getInstance().setValue(rowKey, 'originalRole', row.role);
                     }

                  }
               })
               .catch((error) => {
                  console.error('Error updating user:', error);
               });
            }
         });
      }
   }


   useEffect(() => {
      if (session) {
         fetch(process.env.API_URL + '/users', {
            method: 'GET',
            headers: {
               Authorization: session.accessToken
            },
            cache: 'no-cache'
         })
         .then((response) => response.json())
         .then((data) => {
            setUsers(data);
         })
         .catch((error) => {
            console.error('Error fetching users:', error);
         });
      }
   }, [session]);

   useEffect(() => {
      gridRef?.current?.getInstance().on('dblclick', (ev: any) => {
         if (ev.targetType === 'cell') {
            const rowKey = ev.rowKey;
            const row = gridRef.current?.getInstance().getRow(rowKey);
            if (row?._attributes?.checked) {
               gridRef.current?.getInstance().uncheck(rowKey);
            } else {
               gridRef.current?.getInstance().check(rowKey);
            }
         }
      });
   }, [users]);

   if (users) {
      const data: OptRow[] = users.map((user) => ({
         name: user.name,
         email: user.email,
         role: user.role,
         originalRole: user.role
      }));

      const columns: OptColumn[] = [
         {name: 'name', header: 'Name', sortable: true},
         {name: 'email', header: 'Email', sortable: true},
         {
            name: 'role',
            header: 'Role',
            filter: 'select',
            editor: {
               type: 'radio',
               options: {
                  listItems: [
                     {text: 'CUSTOMER', value: 'CUSTOMER'},
                     {text: 'EMPLOYEE', value: 'EMPLOYEE'},
                     {text: 'ADMIN', value: 'ADMIN'},
                  ],
               },
            },
         }
      ];

      return (<div className={"flex w-full flex-col md:flex-row lg:flex-col"}>
         <div className={"flex-auto grow"}>
            <Grid ref={gridRef} data={data} columns={columns}
                  columnOptions={{resizable: true}} rowHeaders={['checkbox']}
                  header={{align: 'left', columns: [{name: '_checked', align: 'center'}]}}
                  scrollX={false} scrollY={false}/>
         </div>
         <div className={"flex grow-0 flex-row md:flex-col lg:flex-row gap-3 mt-3 md:ml-3 lg:ml-0"}>
            <Button disabled={buttonDisabled()} variant={"danger"}
                    onClick={handleDelete}>Delete Selected Users</Button>
            <Button variant={"warning"} onClick={handleUpdate}>Update Role Changes</Button>
         </div>
      </div>);
   }

   return <div>Loading...</div>;
}

export default UserGrid;