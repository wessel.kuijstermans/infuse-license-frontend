"use client"
import React, {useEffect, useRef, useState} from 'react';
import 'tui-grid/dist/tui-grid.css';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {backOff} from 'exponential-backoff';
import Navbar from "@/app/navbar";
import {useSession} from "next-auth/react";
import {User} from "@/app/lib/User";
import {roleEnum} from "@/app/lib/Roles"
import {redirect} from "next/navigation";
import UserGrid from "@/app/users/UserGrid";


const Users: React.FC = () => {
   const api_base_url = process.env.API_URL;
   const {data: session} = useSession();
   const isAdmin = session?.user?.role === 'ADMIN'
   // const [users, setUsers] = useState<User[] | null>(null);
   // const roles = roleEnum
   //
   //
   // const fetchData = async () => {
   //    try {
   //       if (session) {
   //          const response = await backOff(() => fetch(api_base_url + "/users", {
   //             method: 'GET',
   //             headers: {
   //                'Authorization': session.accessToken
   //             },
   //             cache: 'no-cache'
   //          }));
   //          const responseData: User[] = await response.json();
   //          const updatedData = responseData.map((user) => ({
   //             ...user,
   //             selectedRole: user.role,
   //          }));
   //          setUsers(updatedData);
   //       }
   //    } catch (error) {
   //       console.error('Error fetching data:', error);
   //    }
   // };
   //
   //
   // useEffect(() => {
   //    fetchData();
   // }, []);
   if (session && session?.user.role != "CUSTOMER") {
      return (
         <div className={"w-full md:w-auto"}>
            <ToastContainer/>
            <div className={"flex "}>
               <div className={"flex flex-col md:items-center w-screen lg:w-auto"}>
                  <h1 className={"text-4xl p-4"}>User List: </h1>
                  {/*{users ? (*/}
                     <div className={"flex justify-center w-screen"}>
                        <div className={"tui-grid m-2 lg:m-0 w-full lg:w-2/5"}>
                           <UserGrid/>
                        </div>
                     </div>
                  {/*) : (*/}
                  {/*   <div className={"pt-20 flex justify-center"}>*/}
                  {/*      <img src="https://i.gifer.com/ZZ5H.gif" alt="Loading..." width="75"/>*/}
                  {/*   </div>*/}
                  {/*)}*/}
               </div>
            </div>
         </div>
      );
   } else {
      return (
         redirect("/")
      )
   }
}

export default Users;