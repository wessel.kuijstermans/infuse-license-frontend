// HomePage.tsx
"use client"
import React, {useEffect, useState} from 'react';
import Navbar from "@/app/navbar";
import {useSession} from "next-auth/react";

const HomePage: React.FC = () => {
   const {data: session} = useSession();

   return (
         <div>
            <div className="flex flex-col text-center items-center justify-center pt-10">
               {session && session.user?.name && (
                  <span className="text-4xl">Hello {session.user.name}</span>
               )}
               <span className="text-4xl">Welcome to Infuse!</span>
               {session && session.user.firstLogin && !session.user.image && (
                  <div>
                     <br/>
                     <span className="text-2xl">Please check your inbox to verify your e-mail address</span>
                  </div>
               )}
            </div>
         </div>
   );
};

export default HomePage;