"use client";
// app/not-found.tsx
import Link from 'next/link';
import React from "react";
import {usePathname} from "next/navigation";

const NotFound: React.FC = () => {
   const path = usePathname();
   return (
      <>
         <div className='w-4/5 mx-auto mt-20 flex flex-col justify-center items-center space-y-4'>
            <h1 className='text-4xl font-semibold'>404 - Page Not Found</h1>
            <h4 className='py-10 text-lg text-red-500 font-semibold'>
               The path `{path}` does not exist
            </h4>
            <div className='space-x-4'>
               <Link
                  className='underline text-blue-600 hover:text-red-500 duration-300'
                  href='/'
               >
                  Back to Home
               </Link>
            </div>
         </div>
      </>
   );
}
export default NotFound;