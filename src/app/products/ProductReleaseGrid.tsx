"use client"

import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow, OptCellRenderer} from 'tui-grid/types/options';
import React, {useState, useEffect, useRef} from 'react';
import {ProductRelease} from "@/app/lib/ProductRelease";

interface IProps {
    releases: ProductRelease[] | null
}

const ProductReleaseGrid = ({releases}: IProps) => {
    const gridRef = useRef<Grid>(null);


    if (releases != undefined) {
        const data: OptRow[] = releases.map((productRelease) => ({
            release: productRelease.name
        }));

        const columns: OptColumn[] = [
            {name: 'release', header: 'Number', sortable: true}
        ];

        return (
            <div className={"w-full"}>
                <Grid ref={gridRef} data={data} columns={columns}
                      header={{align: 'left'}} scrollX={false} scrollY={false}/>
            </div>
        )
    }

    return <div>Loading...</div>
}

export default ProductReleaseGrid;