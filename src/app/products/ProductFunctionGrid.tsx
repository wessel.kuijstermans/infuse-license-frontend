"use client"

import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow, OptCellRenderer} from 'tui-grid/types/options';
import React, {useState, useEffect, useRef} from 'react';
import {Function} from "@/app/lib/Function";
import {LicenseFunction} from "@/app/lib/LicenseFunction";

interface IProps {
    productFunctions: Function[] | null
}

const ProductFunctionGrid = ({productFunctions}: IProps) => {
    const gridRef = useRef<Grid>(null);

    if (productFunctions != undefined) {
        const data: OptRow[] = productFunctions.map((productFunction) => ({
            id: productFunction.id,
            function: productFunction.name
        }));

        const columns: OptColumn[] = [
            {name: 'function', header: 'Name', sortable: true}
        ];

        return (
            <div className={"w-full"}>
                <Grid ref={gridRef} data={data} columns={columns}
                      header={{align: 'left'}} scrollX={false} scrollY={false}/>
            </div>
        )
    }

    return <div>Loading...</div>;
}

export default ProductFunctionGrid;