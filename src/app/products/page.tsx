'use client'

import 'tui-grid/dist/tui-grid.css';
import 'react-toastify/dist/ReactToastify.css';
import React, {useEffect, useState} from "react";
import {Product} from '@/app/lib/Product'
import {useSession} from "next-auth/react";
import SelectCard from "@/app/components/elements/SelectCard";
import ProductFunctionGrid from "@/app/products/ProductFunctionGrid";
import ProductReleaseGrid from "@/app/products/ProductReleaseGrid";
import TextBox from "@/app/components/elements/TextBox";
import Button from "@/app/components/elements/Button";
import {toast, ToastContainer} from "react-toastify";

const ProductsPage: React.FC = () => {
    const {data: session} = useSession()
    const [products, setProducts] = useState<Product[] | null>(null)
    const [selectedProduct, setSelectedProduct] = useState<Product | null>(null)
    const [creatingFunction, setCreatingFunction] = useState<boolean>(false)
    const [creatingRelease, setCreatingRelease] = useState<boolean>(false)

    function fetchData() {
        if (session) {
            fetch(process.env.API_URL + '/products', {
                method: "GET",
                headers: {
                    Authorization: session.accessToken
                }
            }).then((response) => response.json().then((data) => {
                setProducts(data)
            }))
        }
    }

    useEffect(() => {
        fetchData();
    }, [session]);

    useEffect(() => {
        if (products) {
            let index = 0
            if (selectedProduct) {
                index = selectedProduct.id - 1
            }
            setSelectedProduct(products[index])
        }
    }, [products]);

    function selectCardOnClick(index: number) {
        if (products) {
            setSelectedProduct(products[index])
            setCreatingFunction(false)
            setCreatingRelease(false)
        }
    }

    function isSelected(objectName: string): boolean {
        return selectedProduct != null && selectedProduct.name == objectName
    }

    function onSubmitFunction() {
        const functionName = document.getElementById("function") as HTMLInputElement;
        if (functionName.value.length == 0) {
            toast.error("Cannot insert empty function name", {autoClose: 2000})
            return
        }
        if (session) {
            fetch(`${process.env.API_URL}/functions/${functionName.value}/${selectedProduct?.name}`, {
                method: "POST",
                headers: {
                    Authorization: session?.accessToken
                }
            }).then((response) => {
                if (response.ok) {
                    toast.success("Function Successfully Added", {autoClose: 2000})
                    fetchData()
                } else {
                    toast.error("Something Went Wrong", {autoClose: 2000})
                }
            })
            setCreatingFunction(false)
        }
    }

    function onSubmitRelease() {
        const release = document.getElementById("release") as HTMLInputElement;
        if (release.value.length == 0) {
            toast.error("Cannot Insert Empty Release Version", {autoClose: 2000})
            return
        }
        if (session) {
            fetch(`${process.env.API_URL}/product-releases/${release.value}/${selectedProduct?.name}`, {
                method: "POST",
                headers: {
                    Authorization: session?.accessToken
                }
            }).then((response) => {
                if (response.ok) {
                    toast.success("Release Successfully Added", {autoClose: 2000})
                    fetchData()
                } else {
                    toast.error("Something Went Wrong", {autoClose: 2000})
                }
            })
            setCreatingRelease(false)
        }
    }


    if (products) {
        return (
            <div className={"m-5"}>
                <ToastContainer/>
                <div className={"grid grid-flow-col justify-stretch space-x-3"}>
                    {products.map(function (object, i) {
                        return (<div className={"cursor-pointer"} onClick={() => selectCardOnClick(i)} key={object.id}>
                            <SelectCard
                                // @ts-ignore
                                selected={isSelected(object.name)}
                                value={object.name}/>
                        </div>)
                    })}
                </div>
                {selectedProduct && (
                    <div className={"flex flex-row justify-between space-x-5 mt-5"}>
                        <div className={"w-1/2"}>
                            <div className={"flex justify-between mb-3"}>
                                <span className={"font-bold text-md"}>Functions</span>
                                <Button onClick={() => setCreatingFunction(true)}
                                        hidden={creatingFunction}
                                        variant={"success"}>Create</Button>
                            </div>
                            {creatingFunction ? (
                                <div className={"flex flex-row items-end space-x-3"}>
                                    <TextBox id={"function"} className={"w-auto"} labelText={"Function Name"}/>
                                    <Button onClick={() => onSubmitFunction()} className={"h-fit"}
                                            variant={"success"}>Submit</Button>
                                    <Button onClick={() => setCreatingFunction(false)} className={"h-fit"}
                                            variant={"danger"}>Cancel</Button>
                                </div>) : (
                                <div className={"tui-grid"}>
                                    {/*@ts-ignore*/}
                                    <ProductFunctionGrid productFunctions={selectedProduct.functions}/>
                                </div>)}

                        </div>
                        <div className={"w-1/2"}>
                            <div className={"flex justify-between mb-3"}>
                                <span className={"font-bold text-md"}>Releases</span>
                                <Button onClick={() => setCreatingRelease(true)}
                                        hidden={creatingRelease}
                                        variant={"success"}>Create</Button>
                            </div>
                            {creatingRelease ? (
                                <div className={"flex flex-row items-end space-x-3"}>
                                    <TextBox id={"release"} className={"w-auto"} labelText={"Release Version"}/>
                                    <Button onClick={() => onSubmitRelease()} className={"h-fit"}
                                            variant={"success"}>Submit</Button>
                                    <Button onClick={() => setCreatingRelease(false)} className={"h-fit"}
                                            variant={"danger"}>Cancel</Button>
                                </div>) : (
                                <div className={"tui-grid"}>
                                    {/*@ts-ignore*/}
                                    <ProductReleaseGrid releases={selectedProduct.productReleases}/>
                                </div>)}

                        </div>
                    </div>
                )}
            </div>
        )
    }

}

export default ProductsPage