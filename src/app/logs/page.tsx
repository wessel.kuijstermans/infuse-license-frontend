"use client"
import React, {useEffect, useState} from "react";
import 'tui-grid/dist/tui-grid.css';
import Navbar from "@/app/navbar";
import {useSession} from "next-auth/react";
import {Log} from "@/app/lib/Log";
import {useRouter} from "next/navigation";
import {ToastContainer} from "react-toastify";
import {Message} from "@/app/lib/Message";
import LogGrid from "@/app/logs/LogGrid";

const Logs: React.FC = () => {
   const router = useRouter()
   const {data: session} = useSession();
   const isEmployeeOrAdmin = session?.user.role === "EMPLOYEE" || session?.user.role === "ADMIN";
   const isPhonePortrait = window.innerWidth < 768;
   const [logs, setLogs] = useState<Log[] | null>(null);

   const fetchData = async () => {
      if (isEmployeeOrAdmin) {
         const response = await fetch(process.env.API_URL + "/logs", {
            method: 'GET',
            headers: {
               'Authorization': session.accessToken
            },
            cache: 'no-cache'
         });
         const responseData: Log[] = await response.json();
         setLogs(responseData);
      }
   }

   useEffect(() => {
      if (isEmployeeOrAdmin) {
         fetchData();
      }
   }, [isEmployeeOrAdmin, router]);

   useEffect(() => {
      const handleOrientationChange = () => {
         location.reload();
      };

      window.addEventListener('resize', handleOrientationChange);

      return () => {
         window.addEventListener('resize', handleOrientationChange);
      };
   }, []);

   function matchMessages(log: Log) {
      const messageCheckBoxes = document.getElementsByName("messagefilter") as NodeListOf<HTMLInputElement>;
      const messageFilters: string[] = []
      messageCheckBoxes.forEach((filter) => {
         if (filter.checked) {
            messageFilters.push(filter.value)
         }
      })
      if (messageFilters.length === 0) {
         return true;
      } else {
         return messageFilters.some((filter) => log.message.includes(filter))
      }
   }

   function matchIp(log: Log) {
      const ipInput = document.getElementById("ipfilter") as HTMLInputElement;
      if (ipInput?.value.trim().length == 0) {
         return true
      } else {
         return log.ipAdress === ipInput.value.trim();
      }
   }

   function matchLicense(log: Log) {
      const licenseInput = document.getElementById("licensefilter") as HTMLInputElement;
      if (licenseInput?.value.trim().length == 0) {
         return true
      } else {
         return log.licenseId.includes(licenseInput.value.trim());
      }

   }


   function parseDate(createdAt: number[]) {
      const [year, month, day, hour, minute, second] = createdAt;
      return new Date(year, month - 1, day, hour, minute, second);
   }

   function matchFilters(log: Log): boolean {
      return matchMessages(log) && matchIp(log) && matchLicense(log);
   }

   return (
      <div className={"w-full md:w-auto"}>
         <ToastContainer/>
         <div className={"flex flex-row mt-5 w-full lg:w-auto md:justify-center"}>

            {/*<div className={"flex flex-col"}>*/}
            {/*   <h1 className={"text-4xl p-4"}>Logs: </h1>*/}
            {/*   <div className={"flex"}>*/}
            {/*      <table className={"w-fit mx-2 lg:w-auto"}>*/}
            {/*         <thead>*/}
            {/*         <tr>*/}
            {/*            <th>License</th>*/}
            {/*            <th>Expires</th>*/}
            {/*            <th>IP</th>*/}
            {/*            <th>Message</th>*/}
            {/*            <th>Write Time</th>*/}
            {/*         </tr>*/}
            {/*         </thead>*/}
            {/*         <tbody>*/}
            {/*         {logs?.map((log) => (*/}
            {/*            <tr hidden={!matchFilters(log)} key={log.id}>*/}
            {/*               <td>{log.licenseId}</td>*/}
            {/*               <td>{new Date(log.licenseExpirationDate).toLocaleDateString()}</td>*/}
            {/*               <td>{log.ipAdress}</td>*/}
            {/*               <td>{log.message}</td>*/}
            {/*               <td>{parseDate(log.createdAt).toLocaleDateString() + " " + parseDate(log.createdAt).toLocaleTimeString()}</td>*/}

            {/*            </tr>*/}
            {/*         ))}*/}
            {/*         </tbody>*/}
            {/*      </table>*/}
            {/*   </div>*/}
            {/*</div>*/}
            {/*<div>*/}
            {/*   <div className={"flex flex-col w-auto pr-2"}>*/}
            {/*      <h1 className={"text-4xl p-4"}>Filters: </h1>*/}
            {/*      <div className={"flex flex-col gap-2 border-s-2 border-gray-500 border-opacity-50 pl-3"}>*/}
            {/*         <div className={"flex flex-col gap-2"}>*/}
            {/*            <span>Message</span>*/}
            {/*            <div className={"flex flex-col gap-2"}>*/}
            {/*               <div className={"flex gap-2"}>*/}
            {/*                  <input id={"created"} onChange={router.refresh} type="checkbox" name="messagefilter"*/}
            {/*                         value={Message.KEY_CREATED}/>*/}
            {/*                  <label htmlFor={"created"}>{Message.KEY_CREATED}</label>*/}
            {/*               </div>*/}
            {/*               <div className={"flex gap-2"}>*/}
            {/*                  <input id={"extended"} onChange={router.refresh} type="checkbox" name="messagefilter"*/}
            {/*                         value={Message.KEY_EXTENDED}/>*/}
            {/*                  <label htmlFor={"extended"}>{Message.KEY_EXTENDED}</label>*/}
            {/*               </div>*/}
            {/*               <div className={"flex gap-2"}>*/}
            {/*                  <input id={"validated"} onChange={router.refresh} type="checkbox" name="messagefilter"*/}
            {/*                         value={Message.KEY_VALIDATED}/>*/}
            {/*                  <label htmlFor={"validated"}>{Message.KEY_VALIDATED}</label>*/}
            {/*               </div>*/}
            {/*               <div className={"flex gap-2"}>*/}
            {/*                  <input id={"not_validated"} onChange={router.refresh} type="checkbox"*/}
            {/*                         name="messagefilter"*/}
            {/*                         value={Message.KEY_NOT_VALIDATED}/>*/}
            {/*                  <label htmlFor={"not_validated"}>{Message.KEY_NOT_VALIDATED}</label>*/}
            {/*               </div>*/}
            {/*               <div className={"flex gap-2"}>*/}
            {/*                  <input id={"revoked"} onChange={router.refresh} type="checkbox" name="messagefilter"*/}
            {/*                         value={Message.KEY_REVOKED}/>*/}
            {/*                  <label htmlFor={"revoked"}>{Message.KEY_REVOKED}</label>*/}
            {/*               </div>*/}
            {/*            </div>*/}
            {/*         </div>*/}
            {/*         <div className={"flex flex-col w-fit gap-2"}>*/}
            {/*            <span>IP</span>*/}
            {/*            <input onInput={router.refresh} className={"text-black"} type="text" id="ipfilter"/>*/}
            {/*         </div>*/}
            {/*         <div className={"flex flex-col w-fit gap-2"}>*/}
            {/*            <span>License</span>*/}
            {/*            <input onInput={router.refresh} className={"text-black"} type="text" id="licensefilter"/>*/}
            {/*         </div>*/}
            {/*      </div>*/}
            {/*   </div>*/}
            {/*</div>*/}
            <div className={"flex justify-center w-screen"}>
               <div className={"tui-grid m-2 lg:m-0 w-full lg:w-2/5"}>
                  <LogGrid/>
               </div>
            </div>
         </div>
      </div>
   )
}

export default Logs;