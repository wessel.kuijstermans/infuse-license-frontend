import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow, OptCellRenderer} from 'tui-grid/types/options';
import {User} from '@/app/lib/User';
import React, {useState, useEffect, useRef} from 'react';
import {useSession} from 'next-auth/react';
import Button from "@/app/components/elements/Button";
import {useRouter} from "next/navigation";
import {Log} from "@/app/lib/Log";

const LogGrid: React.FC = () => {
   const router = useRouter();
   const {data: session} = useSession();
   const [logs, setLogs] = useState<Log[] | null>(null);
   const gridRef = useRef<Grid>(null);

   useEffect(() => {
      if (session) {
         fetch(process.env.API_URL + '/logs', {
            headers: {
               Authorization: session.accessToken
            }
         })
         .then((response) => response.json())
         .then((data) => {
            setLogs(data);
         })
         .catch((error) => {
            console.error('Error fetching logs:', error);
         });
      }
   }, [session]);

   function parseDate(expirationDate: Date | number[]) {
      if (expirationDate instanceof Date) {
         return `${expirationDate.getFullYear()}/${expirationDate.getMonth() + 1}/${expirationDate.getDate()}`
      } else {
         const [year, month, day, hour, minute, second] = expirationDate;
         return `${year}/${month}/${day} ${hour}:${minute}:${second}`
      }
   }

   if (logs) {
      const data: OptRow[] = logs.map((log) => ({
         license: log.licenseId,
         expires: parseDate(new Date(log.licenseExpirationDate)),
         ip: log.ipAdress,
         message: log.message,
         writeTime: parseDate(log.createdAt),
      }));

      const columns: OptColumn[] = [
         {name: 'license', header: 'License'},
         {name: 'expires', header: 'Expires', width: 'auto', minWidth: 100, sortable: true},
         {name: 'ip', header: 'Ip Address', width: 'auto', minWidth: 120, sortable: true, filter: 'select'},
         {name: 'message', header: 'Message', width: 'auto', minWidth: 120, filter: 'select'},
         {name: 'writeTime', header: 'Write Time', width: 'auto', minWidth: 150, sortable: true},
      ];

      return (<div className={"flex w-full flex-col md:flex-row lg:flex-col"}>
         <div className={"flex-auto grow"}>
            <Grid ref={gridRef} data={data} columns={columns}
                  columnOptions={{resizable: true}} header={{align: 'left'}} scrollX={false} scrollY={false}/>
         </div>
      </div>);
   }

   return <div>Loading...</div>;
}

export default LogGrid;