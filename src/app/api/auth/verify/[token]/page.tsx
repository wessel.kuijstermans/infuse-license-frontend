"use client"
import React, {useRef, useState} from "react";
import {useParams, useRouter} from "next/navigation";
import Button from "@/app/components/elements/Button";

const Verify: React.FC = () => {
   const {token} = useParams();
   const router = useRouter()
   const [verified, setVerified] = useState(false);

   const onClick = async () => {
      const repsonse = await fetch(process.env.API_URL + "/auth/verify-email?token=" + token, {
         method: "POST",
      })
      if (repsonse.ok) {
         setVerified(true);
      }
   }

   return (
      <div
         className={
            "flex flex-col justify-center items-center  h-screen bg-gradient-to-br gap-1 from-green-200 to-green-300"
         }
      > {verified ?
         <div
            className={"px-7 py-4 w-4/5 sm:w-1/4 xl:w-1/6 shadow text-black bg-white border border-gray-300 rounded-md flex flex-col gap-2"}>
            <span>Your email has been successfully validated!</span>
            <Button variant={"success"} onClick={() => router.push("/")}>Go to home</Button>
         </div>
         :
         <div
            className={"px-7 py-4 w-4/5 sm:w-1/4 xl:w-1/6 shadow text-black bg-white border border-gray-300 rounded-md flex flex-col gap-2"}>
            <Button variant={"success"} onClick={() => onClick()}>Verify Email</Button>
         </div>
      }
      </div>
   )
}

export default Verify