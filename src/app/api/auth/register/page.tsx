"use client";
import TextBox from "@/app/components/elements/TextBox";
import {signIn} from "next-auth/react";
import React from "react";
import Button from "@/app/components/elements/Button";
import {useRouter} from "next/navigation";


const LoginPage = () => {
   const router = useRouter();
   const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/g;


   const onSubmit = async () => {
      const email = document.getElementById("emailInput") as HTMLInputElement;
      const pass = document.getElementById("passInput") as HTMLInputElement;
      const company = document.getElementById("companyInput") as HTMLInputElement;
      const emailSpan = document.getElementById("emailSpan");
      const passSpan = document.getElementById("passSpan");
      const companySpan = document.getElementById("companySpan");
      const validSpan = document.getElementById("validSpan");

      const emailValue = email.value.trim();
      const passValue = pass.value.trim();
      const companyValue = company.value.trim();
      // @ts-ignore
      emailSpan.hidden = emailRegexp.test(emailValue);
      // @ts-ignore
      passSpan.hidden = passValue >= 6;
      if (emailSpan?.hidden && passSpan?.hidden) {
         const result = await signIn("credentials", {
            email: emailValue,
            password: passValue,
            companyName: companyValue,
            redirect: false,
         });
         if (result?.ok) {
            router.push("/")
         } else if (result?.error) {
            // @ts-ignore
            validSpan.hidden = false;
         } else {

         }
      } else {
         // @ts-ignore
         validSpan.hidden = true;
      }

   }

   const onKeyDown = (e: { key: string; }) => {
      if (e.key === "Enter") {
         onSubmit()
      }
   };

   return (
      <div
         className={
            "flex flex-col justify-center items-center h-screen bg-gradient-to-br gap-1 from-green-200 to-green-300"
         }
      >
         <div
            className="px-7 py-4 w-4/5 sm:w-1/4 xl:w-1/6 shadow text-black bg-white border border-gray-300 rounded-md flex flex-col gap-2">
            <a onClick={() => router.back()} className={"text-gray-600 cursor-pointer w-fit pr-4"}>← Back</a>
            <span className={"text-red-600 text-sm font-semibold"} hidden={true}
                  id={"emailSpan"}>Not a valid email</span>
            <TextBox
               id="emailInput"
               labelText="Email"
               onKeyDown={onKeyDown}
            />
            <span className={"text-red-600 text-sm font-semibold"} hidden={true} id={"passSpan"}>Password must be at least 6 characters</span>
            <TextBox
               id="passInput"
               labelText="Password"
               type={"password"}
               onKeyDown={onKeyDown}
            />
            <span className={"text-red-600 text-sm font-semibold"} hidden={true}
                  id={"companySpan"}>Invalid company name</span>
            <TextBox
               id="companyInput"
               labelText="CompanyName"
               type="text"
               onKeyDown={onKeyDown}
            />
            <span className={"text-red-600 text-sm font-semibold"} hidden={true}
                  id={"validSpan"}>Incorrect combination</span>
            <Button variant={"success"} onClick={onSubmit}>Login</Button>
            <a href={"/api/auth/password-reset"}><span className={"flex justify-center text-sm"}>Forgot Password?</span></a>
         </div>
      </div>
   );
};

export default LoginPage;