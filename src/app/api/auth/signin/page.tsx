"use client";
import TextBox from "@/app/components/elements/TextBox";
import {signIn} from "next-auth/react";
import React from "react";
import Button from "@/app/components/elements/Button";
import {useRouter} from "next/navigation";
import Link from "next/link";


const LoginPage = () => {
   const router = useRouter();
   const emailRegexp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/g;


   const onSubmit = async () => {
      const email = document.getElementById("emailInput") as HTMLInputElement;
      const pass = document.getElementById("passInput") as HTMLInputElement;
      const emailSpan = document.getElementById("emailSpan");
      const passSpan = document.getElementById("passSpan");
      const validSpan = document.getElementById("validSpan");

      const emailValue = email.value.trim();
      const passValue = pass.value.trim();
      // @ts-ignore
      emailSpan.hidden = emailRegexp.test(emailValue);
      // @ts-ignore
      passSpan.hidden = passValue >= 6;
      if (emailSpan?.hidden && passSpan?.hidden) {
         const result = await signIn("credentials", {
            email: emailValue,
            password: passValue,
            redirect: false,
         });
         if (result?.ok) {
            router.push("/")
         } else if (result?.error) {
            // @ts-ignore
            validSpan.hidden = false;
         } else {

         }
      } else {
         // @ts-ignore
         validSpan.hidden = true;
      }

   }

   const onKeyDown = (e: { key: string; }) => {
      if (e.key === "Enter") {
         onSubmit()
      }
   };

   const onGoogleSignIn = async () => {
      const span = document.getElementById("googleSpan")
      const result = await signIn('google', {callbackUrl: '/'});
      // @ts-ignore
      span.hidden = result.ok;
   }

   return (
      <div
         className={
            "flex flex-col justify-center items-center h-screen gap-1"
         }
      >
         <div
            className="px-7 py-4 w-4/5 sm:w-1/4 xl:w-1/6 shadow text-black bg-white border border-gray-300 rounded-md flex flex-col gap-2">
            <span className={"text-red-600 text-sm font-semibold"} hidden={true}
                  id={"emailSpan"}>Not a valid email</span>
            <TextBox
               id="emailInput"
               labelText="Email"
               onKeyDown={onKeyDown}
            />
            <span className={"text-red-600 text-sm font-semibold"} hidden={true} id={"passSpan"}>Password must be at least 6 characters</span>
            <TextBox
               id="passInput"
               labelText="Password"
               type={"password"}
               onKeyDown={onKeyDown}
            />
            <span className={"text-red-600 text-sm font-semibold"} hidden={true}
                  id={"validSpan"}>Incorrect combination</span>
            <Button variant={"success"} onClick={onSubmit}>Login</Button>
            <Link href={"/api/auth/password-reset"}><span
               className={"flex justify-center text-sm text-gray-600"}>Forgot Password?</span></Link>
            <Link href={"/api/auth/register"}><span className={"flex justify-center text-sm text-gray-600"}>First time logging
               in?</span></Link>
            <div className={"separator"}>
               or
            </div>
            <button
               className="transition ease-in-out duration-300 flex items-center w-full hover:bg-gray-200 text-sm xl:text-base text-black font-semibold py-2 px-4 rounded border border-black focus:outline-none focus:shadow-outline"
               onClick={onGoogleSignIn}
            >
               <img
                  src={"/google-logo.png"} style={{height: "3vh", marginLeft: "1vw", marginRight: "2vw"}}/>
               <span>Sign in with Google</span>
            </button>
            <span className={"text-red-600 text-sm font-semibold"} hidden={true}
                  id={"googleSpan"}>Something went wrong</span>
         </div>
      </div>
   );
};

export default LoginPage;