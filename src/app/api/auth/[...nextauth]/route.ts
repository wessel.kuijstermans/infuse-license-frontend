import NextAuth from "next-auth/next"
import GoogleProvider from "next-auth/providers/google"
import CredentialsProvider from "next-auth/providers/credentials"
import {fetch} from "next/dist/compiled/@edge-runtime/primitives";
import {Awaitable} from "next-auth";

const api_base_url = process.env.API_URL;

// @ts-ignore
const handler = NextAuth({
   providers: [
      CredentialsProvider({
         name: "credentials",
         credentials: {
            email: {label: "Email", type: "email", placeholder: "yourname@example.com"},
            password: {label: "Password", type: "password"},
            companyName: {label: "Company Name", type: "text"},
         },
         // @ts-ignore
         async authorize(credentials, req) {
            let response = null;
            if (credentials?.companyName != null) {
               // @ts-ignore
               response = await fetch(api_base_url + "/auth/register", {
                  method: "POST",
                  headers: {
                     "content-type": "application/json",
                  },
                  body: JSON.stringify({
                     email: credentials?.email,
                     password: credentials?.password,
                     companyName: credentials?.companyName
                  })
               })
            } else {
               // @ts-ignore
               response = await fetch(api_base_url + "/auth/login", {
                  method: "POST",
                  headers: {
                     "content-type": "application/json",
                  },
                  body: JSON.stringify({email: credentials?.email, password: credentials?.password})
               })
            }

            try {
               const user = await response?.json();
               if (user) {
                  return {
                     id: user.id,
                     name: user.name,
                     email: user.email,
                     role: user.role.name
                  }
               }
            } catch (err) {
               return null;
            }

         }
      }),
      GoogleProvider({
         clientId: process.env.GOOGLE_CLIENT_ID ?? "",
         clientSecret: process.env.GOOGLE_CLIENT_SECRET ?? "",
      })
   ],
   pages: {
      signIn: "signin",
   },
   callbacks: {
      async jwt({token, user}) {
         let jwt = require('jsonwebtoken');
         if (user) {
            const response = await fetch(api_base_url + "/users", {
               method: "POST",
               headers: {
                  "Content-Type": "application/json",
               },
               body: JSON.stringify({email: user.email, name: user.name}),
            });
            const data = await response.json();
            token.id = data.id;
            token.role = data.role;
            token.companyName = data.companyName;
            token.firstLogin = data.firstTimeLogin;
            token.accessToken = jwt.sign({
               email: user.email,
               role: data.role,
            }, process.env.NEXTAUTH_SECRET, {algorithm: 'HS256'});
         }

         return token;
      },
      async session({token, session}) {
         console.log(token);
         if (session?.user) {
            session.user.id = token.id;
            session.user.role = token.role;
            session.user.companyName = token.companyName;
            session.user.firstLogin = token.firstLogin;
            session.accessToken = token.accessToken;
         }
         return session;
      },

   }
});

export {handler as GET, handler as POST};
