"use client"
import React, {useRef} from "react";
import Navbar from "@/app/navbar";
import {ToastContainer} from "react-toastify";
import TextBox from "@/app/components/elements/TextBox";
import Button from "@/app/components/elements/Button";

const PasswordReset: React.FC = () => {
   const email = useRef("");
   const emailRegexp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/g;


   const onSubmit = async () => {
      const textbox = document.getElementById("emailInput") as HTMLInputElement;
      const error = document.getElementById("emailError");
      if (emailRegexp.test(email.current)) {

      } else {
         textbox.style.borderColor = "red";
         // @ts-ignore
         error.hidden = false;
      }
   }

   const onKeyDown = (e: { key: string; }) => {
      if (e.key === "Enter") {
         onSubmit()
      }
   };

   return (
      <div
         className={
            "flex flex-col justify-center items-center  h-screen bg-gradient-to-br gap-1 from-green-200 to-green-300"
         }
      >
         <div
            className="px-7 py-4 w-4/5 sm:w-1/4 xl:w-1/6 shadow text-black bg-white border border-gray-300 rounded-md flex flex-col gap-2">
            <span className={"text-xl text-black"}>Password Reset</span>
            <TextBox
               id="emailInput"
               labelText="Email"
               onChange={(e) => (email.current = e.target.value)}
               onKeyDown={onKeyDown}></TextBox>
            <span id={"emailError"} hidden={true}
                  className={"text-sm text-red-600 font-semibold"}>Email is invalid</span>
            <Button variant={"success"} onClick={onSubmit}>Confirm</Button>
         </div>
      </div>
   )
}

export default PasswordReset