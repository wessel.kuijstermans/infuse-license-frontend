import React from "react";

const VariableCard: React.FC<{ value: string, label: string, className?: string }> = ({value, label, className}) => {
    return (
        <div className={`relative w-[16rem] h-[7rem] py-6 bg-neutral-100 px-8 ${className}`}>
            <div className={"flex items-end gap-2"}>
                <h1 className={"font-bold text-4xl"}>
                    <span>{value}</span>
                </h1>
            </div>
            <h2 className={"font-normal text-neutral-400 text-md"}>
                <span>{label}</span>
            </h2>
        </div>
    );
};

export default VariableCard;