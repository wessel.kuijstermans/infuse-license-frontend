"use client"
import {signIn, signOut, useSession} from "next-auth/react";
import Link from "next/link";
import {useRouter} from "next/navigation";

const SignInButton = () => {
   const router = useRouter()
   const {data: session} = useSession();


   if (session && session.user) {
      return (
         <div className={"block gap-4 w-full"}>
            <div className="flex flex-nowrap w-full p-2 text-gray-900 rounded hover:text-green-600 hover:bg-green-200/25 cursor-pointer" style={{whiteSpace: "nowrap"}}
                 onClick={() => router.push("/profile/" + session.user.id)}>
                {session.user.image &&
                    <img
                        src={session.user.image}
                        alt={"pfp"}
                        className="rounded-full mr-2"
                        style={{height: "25px", width: "auto"}}
                    />
                }
                {session.user.name ?
                  <span>{session.user.name}</span>
                  :
                  <span>{session.user.email}</span>
               }

            </div>

            <button
               onClick={() => signOut()}
               className="flex w-full p-2 text-gray-900 rounded hover:text-green-600 hover:bg-green-200/25">
               Sign Out
            </button>
         </div>
      );
   }
   return (
      <button
         onClick={() => signIn()}
         className="flex w-full p-2 text-gray-900 rounded hover:text-green-600 hover:bg-green-200/25"
      >
         Sign In
      </button>
   )
}

export default SignInButton