import React from "react";

interface IProps extends React.HTMLProps<HTMLDivElement> {
    value: string
}

const SelectCard: React.FC<IProps> = ({value, ...props}) => {
    return (
        <div
            className={"flex justify-center items-center h-[8rem] px-5 py-4 border border-neutral-700 rounded"}>
            <span className={`text-xl ${props.selected && 'font-bold'}`}>{value}</span>
        </div>
    )
}

export default SelectCard