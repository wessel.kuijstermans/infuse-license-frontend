"use client"
import Link from "next/link";
import React, {useState} from "react";
import SignInButton from "@/app/components/elements/SignInButton";
import {useSession} from "next-auth/react";

const Navbar: React.FC = () => {
   const [isMenuOpen, setIsMenuOpen] = useState(false);
   const {data: session} = useSession();
   const toggleMenu = () => {
      setIsMenuOpen(!isMenuOpen);
   };


   return (
      <div>
         <nav className="bg-white border-gray-200">
            <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
               <Link href="/" className="flex items-center lg:ml-20">
                  <img
                     src="/infuse-black.png"
                     className="h-8"
                     alt="Flowbite Logo"
                  />
               </Link>
               <button
                  data-collapse-toggle="navbar-default"
                  type="button"
                  className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2"
                  aria-controls="navbar-default"
                  aria-expanded={isMenuOpen}
                  onClick={toggleMenu}
               >
                  <span className="sr-only">Open main menu</span>
                  <svg
                     className="w-5 h-5"
                     aria-hidden="true"
                     xmlns="http://www.w3.org/2000/svg"
                     fill="none"
                     viewBox="0 0 17 14"
                  >
                     <path
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M1 1h15M1 7h15M1 13h15"
                     />
                  </svg>
               </button>
               <div
                  className={`${
                     isMenuOpen ? "block" : "hidden"
                  } w-full md:block md:w-auto`}
                  id="navbar-default"
               >
                  <ul
                     className="font-medium flex flex-col p-1 sm:mr-2 lg:mr-10  md:p-0 mt-4 border border-gray-100 rounded-lg md:flex-row md:inline-flex md:space-x-8 md:mt-0 md:border-0">
                     <li>
                        <Link
                           href="/"
                           className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-ihomer-green md:p-0"
                           aria-current="page"
                        >
                           Home
                        </Link>
                     </li>
                     <li hidden={!session || session?.user.role === "CUSTOMER"}>
                        <Link
                           href="/users"
                           className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-ihomer-green md:p-0"
                        >
                           Users
                        </Link>
                     </li>
                     <li>
                        <Link
                           href="/licenses"
                           className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-ihomer-green md:p-0"
                        >
                           Licenses
                        </Link>
                     </li>
                     <li hidden={!session || session.user.role === "CUSTOMER"}>
                        <Link
                           href="/logs"
                           className="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-ihomer-green md:p-0"
                        >
                           Logs
                        </Link>
                     </li>
                     <li className={"flex items-center"}>
                        <span className="hidden md:flex text-ihomer-green pr-10">|</span>
                        <SignInButton/>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
   );
};

export default Navbar;