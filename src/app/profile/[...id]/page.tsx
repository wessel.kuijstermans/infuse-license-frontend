"use client"
import React, {useEffect, useState} from "react";
import {useParams} from "next/navigation";
import Navbar from "@/app/navbar";
import {useSession} from "next-auth/react";
import {User} from "@/app/lib/User";
import {fetchData} from "next-auth/client/_utils";
import Image from "next/image";
import TextBox from "@/app/components/elements/TextBox";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


const ProfilePage: React.FC = () => {
    const {id} = useParams()
    const {data: session} = useSession()
    const [user, setUser] = useState<User | null>(null)
    const [isEditing, setIsEditing] = useState<boolean>(false)


    const fetchData = async () => {
        if (session) {
            const response = await fetch(process.env.API_URL + "/users/" + id, {
                method: 'GET',
                headers: {
                    'Authorization': session.accessToken
                }
            })
            const responseData = await response.json()
            console.log(responseData)
            setUser(responseData)
        }
    }

    useEffect(() => {
        if (session) {
            fetchData()
        }
    }, [])


    async function updateUser(name: string, company: string) {
        if (session) {
            const response = await fetch(process.env.API_URL + "/users/" + id, {
                method: 'PUT',
                headers: {
                    'Authorization': session.accessToken,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({name: name, companyName: company})
            })
            const responseData = await response.json()
            setUser(responseData)
        }
    }

    const toggleEditing = async (doCall: boolean) => {
        // Check if already editing
        if (isEditing && doCall) {
            try {
                // Perform update operation with the new name and company values
                const name = document.getElementById("nameInput") as HTMLInputElement;
                const company = document.getElementById("companyInput") as HTMLInputElement;
                if (name.value === "" && company.value === "") {
                    toast.error("Values may not be empty", {autoClose: 2000});
                    return;
                }
                await updateUser(name.value, company.value);
                // Display success message
                toast.success("Profile updated successfully.", {autoClose: 2000});
            } catch (error) {
                // Display error message
                toast.error("Failed to update profile. Please try again", {autoClose: 2000});
            }
        }
        // Toggle the editing state
        setIsEditing(!isEditing);
    };

    const onKeyDown = (e: { key: string; }) => {
        if (e.key === "Enter") {
            toggleEditing(true)
        }
    };

    return (
        <div>
            <ToastContainer/>
            <div className={"flex justify-center"}>
                <div className={"flex-col gap-5"}>
                    <h1 className={"text-4xl py-5"}>Profile Page</h1>
                    <p>id: {id}</p>
                    {isEditing ?
                        <div><span>name: </span><TextBox onKeyDown={onKeyDown} className={"text-black"} id={"nameInput"}
                                                         defaultValue={user?.name}/>
                        </div> :
                        <p>name: {user?.name}</p>}
                    <p>email: {user?.email}</p>
                    <p>role: {user?.role}</p>
                    {isEditing ? <div><span>company: </span> <TextBox onKeyDown={onKeyDown} className={"text-black"}
                                                                      id={"companyInput"}
                                                                      defaultValue={user?.companyName}/></div> :
                        <p>company: {user?.companyName}</p>}
                    {!isEditing ? <div className={" pt-3"}>
                            <button onClick={() => toggleEditing(true)}>
                                <Image className={"bg-orange-500 rounded"} src={"/pencil-icon-white.png"} alt={"Edit"}
                                       width={50}
                                       height={50}/></button>
                        </div> :
                        <div className={"flex gap-5 pt-3"}>
                            <button onClick={() => toggleEditing(true)}>
                                <Image className={"bg-green-500 rounded p-1"} src={"/check-mark.png"} alt={"confirm"}
                                       width={50}
                                       height={50}/></button>
                            <button onClick={() => toggleEditing(false)}><Image className={"bg-red-500 rounded p-2"}
                                                                                src={"/delete-icon-black.png"}
                                                                                alt={"cancel"} width={50} height={50}/>
                            </button>
                        </div>}
                </div>
            </div>
        </div>)
}

export default ProfilePage;