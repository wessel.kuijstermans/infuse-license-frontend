"use client";
import React, {useEffect, useRef, useState} from "react";
import Navbar from "@/app/navbar";
import TextBox from "@/app/components/elements/TextBox";
import Button from "@/app/components/elements/Button";
import {useSession} from "next-auth/react";
import {useRouter} from "next/navigation";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {Product} from "@/app/lib/Product";
import SelectCard from "@/app/components/elements/SelectCard";
import {Company} from "@/app/lib/Company";
import {LicenseRequestDTO} from "@/app/lib/LicenseRequestDTO"
import {ProductRelease} from "@/app/lib/ProductRelease";
import * as timers from "timers";


const LicenseRequest: React.FC = () => {
    const emailRegex: RegExp = new RegExp("^[^@]+@[^@]+\.[^@]+$")
    const router = useRouter();
    const {data: session} = useSession();
    const versions = document.getElementsByName("version") as NodeListOf<HTMLInputElement>;
    const durations = document.getElementsByName("duration") as NodeListOf<HTMLInputElement>;
    const [companies, setCompanies] = useState<Company[] | null>(null)
    const [products, setProducts] = useState<Product[] | null>(null);
    const [selectedProduct, setSelectedProduct] = useState<Product | null>(null)
    const [isSubscription, setIsSubscription] = useState<boolean>(false)
    const [loading, setLoading] = useState<boolean>(false)

    useEffect(() => {
        if (session) {
            fetch(process.env.API_URL + '/products', {
                method: "GET",
                headers: {
                    Authorization: session?.accessToken
                }
            }).then(response => {
                if (!response.ok) {
                    toast.error("Could not load products", {
                        autoClose: 2000
                    })
                }
                response.json().then(data => {
                    setProducts(data)
                })
            })
        }
    }, []);

    useEffect(() => {
        if (session) {
            fetch(process.env.API_URL + '/companies', {
                method: "GET",
                headers: {
                    Authorization: session?.accessToken
                }
            }).then(response => {
                if (!response.ok) {
                    toast.error("Could not load companies", {
                        autoClose: 2000
                    })
                }
                response.json().then(data => {
                    setCompanies(data)
                })
            })
        }
    }, []);

    function setDefaultStartDate(): string {
        const today = new Date();
        return `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`
    }

    function getSelectedValue(list: NodeListOf<HTMLInputElement>): string | number | undefined {
        let value = undefined
        list.forEach((item) => {
            if (item.checked) {
                value = item.value;
            }
        })
        return value;
    }


    const onSubmit = async () => {
        setLoading(true)
        if (companies && selectedProduct?.productReleases) {
            const selectedFunctions: Function[] = []
            const email = document.getElementById("customerEmail") as HTMLInputElement
            const selectCompany = document.getElementById("selectCompany") as HTMLSelectElement
            const startDatePicker = document.getElementById("startDatePicker") as HTMLInputElement
            const subscriptionBased = document.getElementById("subscription") as HTMLInputElement
            const monthsInput = document.getElementById("monthsPicker") as HTMLInputElement
            const selectRelease = document.getElementById("selectRelease") as HTMLSelectElement


            const company = companies[selectCompany.selectedIndex - 1]
            if (company.name.startsWith("-")) {
                console.log("error")
                toast.error("Please select a company", {autoClose: 2000})
                return
            }
            if (!emailRegex.test(email.value)) {
                console.log("error")
                toast.error("Please enter a valid email", {autoClose: 2000})
            }
            const startDate: Date = new Date(startDatePicker.value)
            let months = 0
            if (subscriptionBased.checked) {
                months = +monthsInput.value
            }
            const maxRelease = selectedProduct.productReleases[selectRelease.selectedIndex]

            if (selectedProduct?.functions) {
                const functions = document.getElementsByName("functions") as NodeListOf<HTMLInputElement>
                for (let i = 0; i < selectedProduct.functions.length; i++) {
                    if (functions.item(i).checked) {
                        selectedFunctions.push(selectedProduct.functions[i])
                    }
                }
            }

            const dto: LicenseRequestDTO = {
                email: email.value,
                months: months,
                company: company,
                functions: selectedFunctions,
                maxRelease: maxRelease,
                startDate: startDate
            }

            if (session) {
                fetch(process.env.API_URL + "/licenses", {
                    method: "POST",
                    headers: {
                        "Authorization": session.accessToken,
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(dto)
                }).then(response => {
                    setLoading(false)
                    if (response.ok) {
                        toast.success("License Successfully Created", {autoClose: 2000})
                    } else {
                        toast.error("Something went wrong", {autoClose: 2000})
                    }
                })
            }
        }
    }

    const onKeyDown = (e: { key: string; }) => {
        if (e.key === "Enter") {
            onSubmit()
        }
    };

    function selectCardOnClick(i: number) {
        if (products) {
            setSelectedProduct(products[i])
        }
    }

    function isSelected(name: string): boolean {
        return selectedProduct?.name == name
    }

    if (session?.user && products) {
        return (
            <div className={"flex m-3"}>
                <ToastContainer/>
                <div className={"w-full"}>
                    <h1 className={"text-xl my-2"}>Request a License</h1>
                    <div className={"flex flex-col w-[20rem] space-y-2"}>
                        <TextBox className={"w-full text-md"} labelText={"Customer Email"} id={"customerEmail"}/>
                        {companies && <div className={"flex flex-col space-y-2"}>
                            <label className={"text-sm text-gray-600"}>Company</label>
                            <select id={"selectCompany"}
                                    className={"appearance-none rounded-md border border-slate-400 bg-slate-50"}>
                                <option>{"-please select a company-"}</option>
                                {companies.map(function (object, i) {
                                    return (<option key={i}>
                                        {object.name}
                                    </option>)
                                })}
                            </select></div>}
                        <label className={"text-sm text-gray-600"}>Start Date</label>
                        <input className={"rounded-md border border-slate-400 bg-slate-50"} type={"date"}
                               defaultValue={setDefaultStartDate()}
                               id={"startDatePicker"}/>

                        <div className={"flex flex-col"}>
                            <div className={"flex flex-row h-[2rem] justify-between items-center"}>
                                <label className={"text-sm text-gray-600"} htmlFor={"subscription"}>Subscription
                                    Based: </label>
                                <input className={"appearance-none rounded mr-2"} id={"subscription"} type={"checkbox"}
                                       checked={isSubscription}
                                       onChange={() => setIsSubscription(!isSubscription)}/>
                            </div>
                            <div hidden={!isSubscription} className={"flex flex-col m-0"}>
                                <label hidden={!isSubscription} className={"text-sm text-gray-600"}>Months</label>
                                <input
                                    hidden={!isSubscription}
                                    className={`rounded-md border border-slate-400 bg-slate-50`}
                                    type={"number"}
                                    id={"monthsPicker"}/>
                            </div>
                        </div>
                    </div>
                    <div className={"flex flex-row mt-3 space-x-5"}>
                        {products.map(function (object, i) {
                            return (<div className={"cursor-pointer"} onClick={() => selectCardOnClick(i)}
                                         key={object.id}>
                                <div
                                    className={`rounded-md p-5 border border-slate-400  ${isSelected(object.name) && 'font-bold bg-slate-50'}`}>{object.name}</div>
                            </div>)
                        })}
                    </div>
                    <div className={"flex flex-col mt-4 w-[20rem]"}>
                        {selectedProduct?.productReleases && <div className={"flex flex-col space-y-2"}>
                            <label className={"text-sm text-gray-600"}>Max Supported Release</label>
                            <select id={"selectRelease"}
                                    className={"appearance-none rounded-md border border-slate-400 bg-slate-50"}>
                                {selectedProduct.productReleases.map(function (object, i) {
                                    return (<option key={i}>
                                        {object.name}
                                    </option>)
                                })}
                            </select></div>}
                        {selectedProduct?.functions && <div>
                            {selectedProduct.functions.map(function (object, i) {
                                return (
                                    <div key={i} className={"items-center space-x-2"}>
                                        <input name={"functions"} id={object.name} className={"appearance-none rounded"}
                                               type={"checkbox"}/>
                                        <label htmlFor={object.name}>{object.name}</label>
                                    </div>
                                )
                            })}
                        </div>
                        }
                    </div>
                    <Button hidden={selectedProduct == null} className={"mt-2"} variant={"success"}
                            onClick={() => onSubmit()}>
                        {loading ? (<div className={"flex space-x-2"}>
                            <span className={"loading loading-spinner loading-md"}/>
                            <span>Loading</span>
                        </div>) : 'Submit'}
                    </Button>
                </div>


            </div>
        )
    }
    return (
        <div>
            Please sign in to see this page
        </div>
    )
}

export default LicenseRequest