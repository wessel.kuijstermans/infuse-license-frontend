import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow, OptCellRenderer} from 'tui-grid/types/options';
import {User} from '@/app/lib/User';
import React, {useState, useEffect, useRef} from 'react';
import {useSession} from 'next-auth/react';
import Button from "@/app/components/elements/Button";
import {Function} from "@/app/lib/Function";
import {useRouter} from "next/navigation";
import {LicenseFunction} from "@/app/lib/LicenseFunction";

interface IProps {
    licenseFunctions: LicenseFunction[] | null
}

const LicenseFunctionGrid = ({licenseFunctions}: IProps) => {
    const gridRef = useRef<Grid>(null);

    function parseDate(date: Date) {
        if (date.getFullYear() != 1970) {
            return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
        }
        return "N/A"
    }

    if (licenseFunctions) {
        const data: OptRow[] = licenseFunctions.map((licenseFunction) => ({
            id: licenseFunction.id,
            functionName: licenseFunction.function.name,
            contractStatus: licenseFunction.contractStatus,
            endDate: parseDate(new Date(licenseFunction.endDate)),
        }));

        const columns: OptColumn[] = [
            {name: 'functionName', header: 'Function', sortable: true},
            {name: 'contractStatus', header: 'Contract Status', filter: 'select', defaultValue: 'N/A'},
            {name: 'endDate', header: 'End Date', sortable: true}
        ];

        return (
            <div className={"w-full"}>
                <Grid ref={gridRef} data={data} columns={columns}
                      header={{align: 'left'}} scrollX={false} scrollY={false}/>
            </div>
        )
    }

    return <div>Loading...</div>;
}

export default LicenseFunctionGrid;