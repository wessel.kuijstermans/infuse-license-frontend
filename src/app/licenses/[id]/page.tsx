"use client"

import 'tui-grid/dist/tui-grid.css';
import React, {useEffect, useRef, useState} from "react";
import {useParams, useRouter} from "next/navigation";
import Button from "@/app/components/elements/Button";
import Navbar from "@/app/navbar";
import Link from "next/link";
import Popup from "reactjs-popup";
import {useSession} from "next-auth/react";
import {LicenseResponseDTO} from "@/app/lib/LicenseResponseDTO";
import Label from "@/app/components/elements/VariableCard";
import LicenseFunctionGrid from "@/app/licenses/[id]/LicenseFunctionGrid";
import VariableCard from "@/app/components/elements/VariableCard";

const LicenseDetails: React.FC = () => {
    const {data: session} = useSession();
    const router = useRouter();
    const {id} = useParams();
    const isEmployeeOrAdmin = session?.user.role === "EMPLOYEE" || session?.user.role === "ADMIN";
    const [license, setLicense] = useState<LicenseResponseDTO | null>(null);
    // @ts-ignore
    const popupRef = useRef<Popup>(null);
    const [loading, setLoading] = useState(true);

    const fetchData = async () => {
        try {
            if (session) {
                const response = await fetch(process.env.API_URL + "/licenses/" + id, {
                    method: 'GET',
                    headers: {
                        'Authorization': session.accessToken
                    }
                });
                const responseData = await response.json();
                setLoading(false);
                if (response.ok) {
                    setLicense(responseData);
                } else {
                    setTimeout(() => {
                        router.push("/licenses")
                    }, 3000)
                }
            }
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    }

    useEffect(() => {
        if (session) {
            fetchData();
        }
    }, [session]);

    useEffect(() => {
        if (!id) {
            router.push("/licenses")
        }
    }, [id])

    const handleClosePopup = () => {
        popupRef.current?.close();
    };

    function parseDate(date: Date) {
        if (date.getFullYear() != 1970) {
            return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
        }
        return "N/A"
    }

    return <div>
        <div className={"flex flex-row justify-between"}>
            <a onClick={() => router.push('/licenses')} className={"m-2 cursor-pointer"}>← Back to Licenses</a>
            <Button className={"mr-5 mt-3"} variant={"warning"}
                    onClick={() => router.push(`/licenses/${license?.id}/edit`)}>Edit</Button>
        </div>
        <div className={"w-full"}>
            {license == null ? (loading ? (
                    <div className={"flex justify-center mt-3"}>
                        <span className={"loading loading-spinner loading-lg"}/>
                    </div>
                ) : (
                    <div className={"flex flex-col items-center mt-3 text-2xl font-bold"}>
                        <h1>License Not Found</h1>
                        <h2>Redirecting to /licenses</h2>
                    </div>
                )
            ) : (
                <div>
                    <div className={"flex flex-row m-3 space-x-3"}>
                        <VariableCard className={"w-auto"} value={license.company}
                                      label={"Company Name"}/>
                        <VariableCard className={"w-auto"} value={license.product} label={"Product"}/>
                        <VariableCard className={"w-auto"} value={parseDate(new Date(license.startDate))}
                                      label={"Start Date"}/>
                        <VariableCard className={"w-auto"} value={parseDate(new Date(license.endDate))}
                                      label={"End Date"}/>
                        <VariableCard className={"w-auto"} value={license.maxRelease} label={"Max Version"}/>
                        <VariableCard className={"w-auto"} value={license.contractStatus} label={"Contract Status"}/>
                    </div>
                    <div className={"tui-grid m-3"}>
                        <LicenseFunctionGrid licenseFunctions={license?.functions}/>
                    </div>
                </div>
            )}
        </div>

    </div>
}

export default LicenseDetails;