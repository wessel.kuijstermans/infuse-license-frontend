'use client'
import React, {useEffect, useState} from "react";
import {useSession} from "next-auth/react";
import {Product} from "@/app/lib/Product";
import {useParams, useRouter} from "next/navigation";
import {LicenseResponseDTO} from "@/app/lib/LicenseResponseDTO";
import {License} from "@/app/lib/License";
import 'react-toastify/dist/ReactToastify.css';
import {toast, ToastContainer} from "react-toastify";
import LicenseFunctionGrid from "@/app/licenses/[id]/LicenseFunctionGrid";
import {ContractStatus} from "@/app/lib/ContractStatus";
import Button from "@/app/components/elements/Button";
import {LicenseFunction} from "@/app/lib/LicenseFunction";

const EditLicense: React.FC = () => {
    const {data: session} = useSession()
    const router = useRouter()
    const [product, setProduct] = useState<Product | null>(null)
    const {id} = useParams()
    const [originalLicense, setOriginalLicense] = useState<LicenseResponseDTO | null>(null)
    let license = {} as License

    const ContractOptions = Object.values(ContractStatus) as string[]

    useEffect(() => {
        if (session) {
            fetch(process.env.API_URL + "/licenses/" + id, {
                method: "GET",
                headers: {
                    "Authorization": session.accessToken
                }
            }).then(response => {
                if (response.ok) {
                    response.json().then(data => {
                        setOriginalLicense(data)
                    })
                } else {
                    toast.error("Could Not Retrieve License", {autoClose: 2000})
                }
            })
        }
    }, []);

    useEffect(() => {
        if (session) {
            fetch(process.env.API_URL + "/products/name/" + originalLicense?.product, {
                method: "GET",
                headers: {
                    "Authorization": session.accessToken
                }
            }).then(response => {
                if (response.ok) {
                    response.json().then(data => {
                        setProduct(data)
                    })
                } else toast.error("Could Not Retrieve Product", {autoClose: 2000})
            })
        }
    }, [originalLicense]);

    function isInLicense(functionToCheck: Function): boolean {
        let response = false
        if (originalLicense?.functions) {
            for (let i = 0; i < originalLicense?.functions?.length; i++) {
                if (originalLicense.functions[i].function.name == functionToCheck.name) {
                    response = true
                }
            }
        }
        return response
    }

    function onSubmit() {
        if (product?.functions && originalLicense && session) {
            const contractStatus = document.getElementById("contractStatus") as HTMLSelectElement
            const maxRelease = document.getElementById("maxRelease") as HTMLSelectElement
            const functions = document.getElementsByName("functions") as NodeListOf<HTMLInputElement>
            const selectedFunctions: LicenseFunction[] = []
            license.id = originalLicense.id
            for (const object of product.productReleases) {
                if (maxRelease.value == object.name) {
                    license.maxRelease = object
                }
            }

            license.contractStatus = contractStatus.value as ContractStatus


            for (let i = 0; i < product.functions.length; i++) {
                if (functions.item(i).checked && functions.item(i).id == product.functions[i].name) {
                    let licenseFunction = {
                        license: {id: license.id},
                        function: product.functions[i]
                    } as unknown as LicenseFunction
                    selectedFunctions.push(licenseFunction)
                }
            }
            license.licenseFunctions = selectedFunctions
            fetch(process.env.API_URL + "/licenses", {
                method: "PUT",
                headers: {
                    "Authorization": session.accessToken,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(license)
            }).then(response => {
                if (response.ok) {
                    toast.success("License Successfully Updated", {autoClose: 2000})
                    router.push('/licenses/' + license.id)
                } else {
                    toast.error("Something went wrong", {autoClose: 2000})
                }
            })
        }
    }

    return (
        <div>
            <ToastContainer/>
            <a onClick={() => router.back()} className={"m-2 cursor-pointer"}>← Back to Licenses</a>
            <div className={"ml-5 mt-4"}>
                {(originalLicense && product) ? (
                    <div>
                        <span
                            className={"text-2xl"}>Editing {originalLicense.company + "'s"} {originalLicense.product} License</span>
                        <div className={"flex flex-row mt-3 space-x-5"}>
                            <div>
                                <span className={"text-xl"}>Functions</span>
                                {product.functions.map(function (object, i) {
                                    return (
                                        <div key={i} className={"space-x-2"}>
                                            <input name={"functions"} id={object.name}
                                                   className={"appearance-none rounded"}
                                                   type={"checkbox"}
                                                   defaultChecked={isInLicense(object)}/>
                                            <label htmlFor={object.name}>{object.name}</label>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className={"flex flex-col"}>
                                <span className={"text-xl"}>Contract Status</span>
                                <select id={"contractStatus"} className={"appearance-none rounded-md"}>
                                    <option>{originalLicense.contractStatus.toUpperCase()}</option>
                                    <option disabled={true}>──────────</option>
                                    {ContractOptions.filter(value => {
                                        return value != originalLicense.contractStatus.toUpperCase()
                                    }).map(function (string) {
                                        return <option key={string}>{string}</option>
                                    })}
                                </select>
                            </div>
                            <div className={"flex flex-col"}>
                                <span className={"text-xl"}>Max Release</span>
                                <select id={"maxRelease"} className={"appearance-none rounded-md"}>
                                    <option>{originalLicense.maxRelease}</option>
                                    <option disabled={true}>──────────</option>
                                    {product.productReleases.filter(value => {
                                        return value.name != originalLicense.maxRelease
                                    }).map(function (object) {
                                        return <option key={object.name}>{object.name}</option>
                                    })}
                                </select>
                            </div>
                            <div className={"flex flex-col space-y-3"}>
                                <Button variant={"success"} onClick={() => onSubmit()}>Confirm</Button>
                                <Button variant={"outline-danger"}
                                        onClick={() => router.push(`/licenses/${originalLicense.id}`)}>Cancel</Button>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>
                        Loading...
                    </div>
                )}
            </div>
        </div>
    )
}

export default EditLicense