import Grid from '@toast-ui/react-grid';
import {OptColumn, OptRow, OptCellRenderer} from 'tui-grid/types/options';
import {User} from '@/app/lib/User';
import React, {useState, useEffect, useRef} from 'react';
import {useSession} from 'next-auth/react';
import Button from "@/app/components/elements/Button";
import {LicenseResponseDTO} from "@/app/lib/LicenseResponseDTO";
import {useRouter} from "next/navigation";

const LicenseGrid: React.FC = () => {
    const router = useRouter();
    const {data: session} = useSession();
    const [licenses, setLicenses] = useState<LicenseResponseDTO[] | null>(null);
    const gridRef = useRef<Grid>(null);

    useEffect(() => {
        if (session) {
            fetch(process.env.API_URL + '/licenses', {
                method: 'GET',
                headers: {
                    Authorization: session.accessToken
                },
                cache: 'no-cache'
            })
                .then((response) => response.json())
                .then((data) => {
                    setLicenses(data);
                })
                .catch((error) => {
                    console.error('Error fetching users:', error);
                });
        }
    }, [session]);

    useEffect(() => {
        gridRef?.current?.getInstance().on('click', (ev: any) => {
            if (ev.targetType === 'cell') {
                const rowKey = ev.rowKey;
                const row = gridRef.current?.getInstance().getRow(rowKey);
                router.push("/licenses/" + row?.id)
            }
        });
    }, [licenses]);

    function parseDate(date: Date) {
        if (date.getFullYear() != 1970) {
            return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
        }
        return "N/A"
    }

    if (licenses) {
        const data: OptRow[] = licenses.map((license) => ({
            id: license.id,
            company: license.company,
            product: license.product,
            startDate: parseDate(new Date(license.startDate)),
            endDate: parseDate(new Date(license.endDate)),
            contractStatus: license.contractStatus,
        }));

        const columns: OptColumn[] = [
            {name: 'company', header: 'Company', sortable: true, filter: 'text'},
            {name: 'product', header: 'Product', filter: 'select'},
            {name: 'startDate', header: 'Start Date', sortable: true},
            {name: 'endDate', header: 'End Date', sortable: true},
            {name: 'contractStatus', header: 'Contract Status', filter: 'select'},
        ];

        return (
            <div className={"w-full"}>
                <Grid ref={gridRef} data={data} columns={columns}
                      header={{align: 'left'}} scrollX={false} scrollY={false}/>
            </div>
        )
    }

    return <div>Loading...</div>;
}

export default LicenseGrid;