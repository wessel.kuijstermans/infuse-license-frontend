"use client"

import 'tui-grid/dist/tui-grid.css';
import React, {useEffect, useState} from "react";
import Navbar from "@/app/navbar";
import {ToastContainer} from "react-toastify";
import {useSession} from "next-auth/react";
import {LicenseResponseDTO} from "@/app/lib/LicenseResponseDTO";
import {useRouter} from "next/navigation";
import Button from "@/app/components/elements/Button";
import Link from "next/link";
import Image from "next/image";
import LicenseGrid from "@/app/licenses/LicenseGrid";

const Licenses: React.FC = () => {
    const {data: session} = useSession();
    const isCustomer = session?.user.role === "CUSTOMER";
    const isEmployeeOrAdmin = session?.user.role === "EMPLOYEE" || session?.user.role === "ADMIN";

    return (
        <div>
            <ToastContainer/>
            <div className={"flex"}>
                {session?.user ? (
                    <div className={"w-full"}>
                        <div className={"flex"}>
                            <h1
                                className={"text-4xl p-4"}>{isEmployeeOrAdmin ? "License Overview" : "My Licenses"}</h1>
                            <div className={"flex flex-1 justify-end"}>
                                <Button className={"h-auto my-5 mr-5"} variant={"success"}>
                                    <Link href={"/licenses/create"}>
                                        Create a License
                                    </Link>
                                </Button>
                            </div>
                        </div>
                        <div className={"flex w-auto tui-grid mx-4"}>
                            <LicenseGrid/>
                        </div>
                    </div>
                ) : (
                    <div>
                        <h1 className={"text-4xl p-4"}>
                            Please sign in to view this page
                        </h1>
                    </div>
                )}
            </div>
        </div>
    )
}

export default Licenses