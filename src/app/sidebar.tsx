"use client"
import Link from "next/link";
import Image from "next/image";
import React from "react";
import SignInButton from "@/app/components/elements/SignInButton";
import {useSession} from "next-auth/react";

const Sidebar: React.FC = () => {
    const {data: session} = useSession();
    return (
        <nav className={"w-full min-w-fit z-40 h-screen"}>
            <div className={"h-full w-full flex flex-col justify-between px-3 py-4 overflow-y-auto bg-gray-100"}>
                <div><Link href={"/"}>
                    <img
                        src="/infuse-black.png"
                        className="h-8"
                        alt="Infuse Logo"
                    />
                </Link>
                    <ul hidden={!session} className={"space-y-1 mt-2 font-medium"}>
                        <li>
                            <Link href={"/licenses"}
                                  className={"flex items-center p-2 text-gray-900 rounded-lg hover:text-green-600 hover:bg-green-200/25"}>
                                <span>Licenses</span>
                            </Link>
                        </li>
                        <li>
                            <Link href={"/companies"}
                                  className={"flex items-center p-2 text-gray-900 rounded-lg hover:text-green-600 hover:bg-green-200/25"}>
                                <span>Companies</span>
                            </Link>
                        </li>
                        <li>
                            <Link href={"/products"}
                                  className={"flex items-center p-2 text-gray-900 rounded-lg hover:text-green-600 hover:bg-green-200/25"}>
                                <span>Products</span>
                            </Link>
                        </li>
                    </ul>
                </div>
                <ul className={"space-y-2 font-medium pt-2 border-t border-gray-300"}>
                    <li>
                        <SignInButton/>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Sidebar;